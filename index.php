<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

		<title>Trial</title>
		<style>
			body{
				margin-top: 40px;
			}
			.card{
				width: 100%;
				margin-top: 10px;
				margin-bottom: 10px;
			}

			h3{
				text-align: center;
			}
		</style>
	</head>
	<body class="row justify-content-center">
		<?php
			include 'conn.php';
			$topClient = $pdo->query("SELECT client,sum(accepted) as Aceites from trial group by client order by sum(accepted)");
			$resCli = $topClient->fetchAll();
			$accepted = $pdo->query("SELECT sum(accepted) as Aceites from trial");
			$res1 = $accepted->fetchAll();
			$rejected = $pdo->query("SELECT sum(refused) as Rejected from trial");
			$res2 = $rejected->fetchAll();
		?>
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-3">
					<form action="import.php" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label>Import File</label>
							<input type="File" name="file" class="form-control">
						</div>
						<button class="btn btn-outline-info">Import</button>
					</form>
					<div>
						<div class="card">
							<h3 class="card-header">
								Accepted transactions
							</h3>
							<h3 class="card-body" style="color: green">
								<?php
									echo $res1[0][0];
								?>
							</h3>
						</div>
						<div class="card">
							<h3 class="card-header">
								Rejected transactions
							</h3>
							<h3 class="card-body" style="color: red">
								<?php
									echo $res2[0][0];
								?>
							</h3>
						</div>
						<div class="card">
							<h3 class="card-header">
								Top Clients
							</h3>
							<h3 class="card-body" style="color: blue">
								<?php
									for( $i = 0 ; $i < count($resCli); $i++ ){
										echo $resCli[$i]['client'];
									}
								?>
							</h3>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th><input type="number" id="cliId" placeholder="Filter ID"><br></th>
								<th></th>
								<th><input type="number" id="dealId" placeholder="Filter ID"></th>
								<th colspan="4"></th>

							</tr>

							<tr>
								<td>
									Client ID
								</td>
								<td>Client</td>
								<td>

									Deal ID
								</td>
								<td>Deal</td>
								<td>Hour</td>
								<td>Accepted</td>
								<td>Refused</td>
							</tr>
						</thead>
						<tbody>
							<?php

								$result = $pdo->query("SELECT * FROM trial");

								while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
									?>
									<tr>
										<td>
											<?php echo $row['client_id'];?>
										</td>
										<td>
											<?php echo $row['client'];?>
										</td>
										<td>
											<?php echo $row['deal_id'];?>
										</td>
										<td>
											<?php echo $row['deal'];?>
										</td>
										<td>
											<?php echo $row['hour'];?>
										</td>
										<td>
											<?php echo $row['accepted'];?>
										</td>
										<td>
											<?php echo $row['refused'];?>
										</td>
									</tr>
									<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function() {
			    var table = $('.table').DataTable({

			    });

				$('#cliId').on( 'keyup', function () {
				    table
			        .columns( 0 )
			        .search( this.value )
			        .draw();
				} );
				$('#dealId').on( 'keyup', function () {
				    table
			        .columns( 2 )
			        .search( this.value )
			        .draw();
				} );

			} );
		</script>
	</body>
</html>
