<?php

	function importFileToSql($file)
	{
		$handle = fopen($file, "r");

		if(empty($handle) === false)
		{
			while(! feof($handle))
			{
				$content = fgetcsv($handle);

				$client = explode('@',$content[0]);
				$deal = explode('#',$content[1]);

				if(!empty($client[1]) || !empty($deal[1]))
				{
					try
					{
						$query = $pdo->prepare("INSERT INTO trial (client,client_id,deal,deal_id,hour,accepted,refused) values (:cli,:cliId,:del,:delId,:dtime,:acc,:ref)");

						$query->bindParam(':cli',$client[0], PDO::PARAM_STR);
						$query->bindValue(':cliId',$client[1]);
						$query->bindParam(':del',$deal[0], PDO::PARAM_STR);
						$query->bindValue(':delId',$deal[1]);
						$query->bindParam(':dtime',$content[2]);
						$query->bindValue(':acc',$content[3]);
						$query->bindValue(':ref',$content[4]);
						$query->execute();

					} catch (PDOException $e) {
						echo $e->getMessage();
						break;
					}
				}
			}
		}

		fclose($handle);
	}

?>
