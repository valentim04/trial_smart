<?php
	if (isset($argv) && count($argv) < 5)
	{
		print "Utilização :\r\n";
		print "php <file path> -s <servidor> -d <basedados> -u <utilizador> -p <password>";
		exit;
	}
	elseif(isset($argv))
	{

		if (!isset($args['s']) || !isset($args['d']) || !isset($args['u']) || !isset($args['p'])){
			print "Utilização :\r\n";
			print "php <file path> -s <servidor> -d <basedados> -u <utilizador> -p <password> -f <ficheiro>";
			exit;
		}

		$server = $args['s'];
		$dbname = $args['d'];
		$user = $args['u'];
		$password = $args['p'];
	}
	else
	{
		$server = "127.0.0.1";
		$dbname = "smart";
		$user = "laravel";
		$password ="laravel";
	}

	// Connect to server
	try{

		$pdo = new PDO("mysql:host=$server", $user, $password);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->query("use $dbname");
		$pdo->query("SELECT * from trial LIMIT 1");

	} catch (PDOException $e) {
		if($e->getCode() == 42000){
			// Cria BD se Não existir, seguidamente cria a tabela
			$dbname = "`".str_replace("`","``",$dbname)."`";
			$pdo->query("CREATE DATABASE IF NOT EXISTS $dbname");
			$pdo->query("use $dbname");
			$pdo->query("CREATE TABLE IF NOT EXISTS `trial` (
				`id` INT NOT NULL AUTO_INCREMENT,
				`client` VARCHAR(100) NOT NULL DEFAULT '0',
				`client_id` INT NOT NULL DEFAULT '0',
				`deal` VARCHAR(100) NOT NULL DEFAULT '0',
				`deal_id` INT NOT NULL DEFAULT '0',
				`hour` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`accepted` INT NOT NULL DEFAULT '0',
				`refused` INT NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			);");
		}elseif ($e->getCode() == "42S02") {
			// Cria Tabela
			$pdo->query("CREATE TABLE IF NOT EXISTS `trial` (
				`id` INT NOT NULL AUTO_INCREMENT,
				`client` VARCHAR(100) NOT NULL DEFAULT '0',
				`client_id` INT NOT NULL DEFAULT '0',
				`deal` VARCHAR(100) NOT NULL DEFAULT '0',
				`deal_id` INT NOT NULL DEFAULT '0',
				`hour` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`accepted` INT NOT NULL DEFAULT '0',
				`refused` INT NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			);");
		}

		else {
			// Se erro for diferente de "BD Inexistente"
			echo "Error : ".$e->getMessage();
		}
	}
?>
